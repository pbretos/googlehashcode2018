package hashCode;

import hashCode.ioUtils.IOUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Start {
  public static void main(String[] args) throws IOException {
    List<String> inputString = IOUtils.ReadFile();
	Boolean firstline = true;
	Simulator simulator = null;
	List<Ride> rides = new ArrayList<Ride>();
	List<Vehicle> vehicles = new ArrayList<Vehicle>();
	
	for (String line : inputString) {
		String[] values = line.split(" ");
		if(firstline) {
			firstline = false;
			simulator = new Simulator(toInt(values[0]),toInt(values[1]),toInt(values[2]),toInt(values[3]),toInt(values[4]),toInt(values[5]));
			for(int i=0;i<simulator.vehicles;i++) {
				vehicles.add(new Vehicle(simulator.steps));
			}
		}else {
			Ride ride = new Ride(rides.size(), toInt(values[0]),toInt(values[1]),toInt(values[2]),toInt(values[3]),toInt(values[4]),toInt(values[5]));
			
			//System.out.println(""+ ride.distance);
			rides.add(ride)	;
		}
		
	}
	System.out.println("vechic : " + simulator.vehicles +  " Total rides: " + rides.size()+  " Total vehicles: " + vehicles.size());
    //rides.sort(null);
	//Arrays.sort(rides.toArray(),Ride.RideDistanceComparator());
	Collections.sort(rides,Ride.RideearliestStartComparator);
	for (Ride ride : rides) {
		//System.out.println(""+ride.id);
	}
	
	
	int vehicleNum = 0;
	for (Ride ride : rides) {
    //for (int i = 0; i < rides.size(); i++) {
		if(ride.active != 0) {
			for(Vehicle vehicle: vehicles) {
//				for (int j = 0; j< rides.size(); j++) {
//					Ride ride2 = rides.get(j);
//					ride2.recalcdistance =  vehicle.distanceToRide(ride2);
//					
//				}
				//System.out.println("test-"+ride.id);
				//Collections.sort(rides,Ride.RideRecalcDistanceComparator);
				//i=-1;
				//System.out.println("test1-"+ride.id);
				
				if (vehicle.fitsRide(ride)) {
				  vehicle.addRide(ride);
				  ride.active = 0;
				  break;
				}else {
					System.out.println("Don't fit -> ride:"+ride.id + " ve: "+vehicleNum);
				}
			}
		}
		ride.active = 0;
	}

	
	
    List<String> finalString = new ArrayList<String>();

    for (Vehicle vehicle : vehicles) {
    	
    	String out = ""; 
    	out += vehicle.ridestodo.size() + " ";
    	for(Ride ride : vehicle.ridestodo) {
    		out +=  ride.id + " ";
    	}
    	out = out.trim();
    	finalString.add(out);
    }
    
    IOUtils.CreateFile(finalString);

  }
  public static int toInt(String value) {
	  return Integer.parseInt(value);
  }
}