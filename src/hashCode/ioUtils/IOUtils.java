package hashCode.ioUtils;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class IOUtils {
//	static String filename= "a_example";
//	static String filename= "b_should_be_easy";
	//static String filename= "c_no_hurry";
//	static String filename= "d_metropolis";
	static String filename= "e_high_bonus";
  
  
  
  public static List<String> ReadFile() throws IOException {
    Path path = FileSystems.getDefault().getPath("inputs", filename+".in");
    List<String> stringifiedInput = Files.readAllLines(path, Charset.defaultCharset());
    return stringifiedInput;
  }

  public static void CreateFile(List<String> inputString) throws IOException {
    try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename+".out"), StandardCharsets.UTF_8))) {
      Path path = FileSystems.getDefault().getPath(filename+".out");
      Files.write(path, inputString, Charset.defaultCharset());
    }
  }

}