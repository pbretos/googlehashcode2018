package hashCode;

import hashCode.ioUtils.IOUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

class Vehicle {
  public int x = 0;
  public int y = 0;
  public int remainingSteps;
  public int currentStep = 0;
  List<Ride> ridestodo = new ArrayList<Ride>();
  public Vehicle(int steps) {
	  this.remainingSteps = steps;
  }
  
  public boolean fitsRide(Ride ride) {
	  int distanceToRideStart = Math.abs((y-ride.startY)) + Math.abs((x - ride.startX));
	  if (distanceToRideStart + ride.distance > remainingSteps ) {
		  return false;
	  }
	  if (currentStep + distanceToRideStart + ride.distance >= ride.lastestEnd ) {
		  return false;
	  }
	  return true;
  }
  public int distanceToRide(Ride ride) {
	  return Math.abs((y-ride.startY)) + Math.abs((x - ride.startX));
	  
  }
  public void addRide(Ride ride) {
	  int distanceToRideStart = Math.abs((y-ride.startY)) + Math.abs((x - ride.startX));
	  remainingSteps = remainingSteps - distanceToRideStart - ride.distance;
	  currentStep = currentStep + distanceToRideStart + ride.distance;
	  x = ride.endX;
	  y = ride.endY;
	  ridestodo.add(ride);
  }
  
}

class Ride {
  public int id;
  public int startX;
  public int startY;
  public int endX;
  public int endY;
  public int earliestStart;
  public int lastestEnd;
  public int distance;
  public int recalcdistance=0;
  public int active = 1;
  public Ride(int id, int startX, int startY, int endX, int endY, int earliestStart, int lastestEnd) {
	this.id = id;
    this.startX = startX;
    this.startY = startY;
    this.endX = endX;
    this.endY = endY;
    this.earliestStart = earliestStart;
    this.lastestEnd = lastestEnd;
    this.distance =( Math.abs((startY - endY)) + Math.abs((startX-endX)))+earliestStart;
  }
  
  public static Comparator<Ride> RideDistanceComparator = new Comparator<Ride>() {

		public int compare(Ride ride1, Ride ride2) {
		
			Integer ridedistance1 = ride1.distance;
			Integer ridedistance2 = ride2.distance;
			
			//ascending order
			//return ridedistance1.compareTo(ridedistance2);
			
			//descending order
			return ridedistance1.compareTo(ridedistance2);
		}
	  };
	  
	  public static Comparator<Ride> RideearliestStartComparator = new Comparator<Ride>() {

			public int compare(Ride ride1, Ride ride2) {
			
				Integer ridedistance1 = ride1.earliestStart;
				Integer ridedistance2 = ride2.earliestStart;
				
				//ascending order
				//return ridedistance1.compareTo(ridedistance2);
				
				//descending order
				return ridedistance1.compareTo(ridedistance2);
			}
		  };
		  
		  public static Comparator<Ride> RideRecalcDistanceComparator = new Comparator<Ride>() {

				public int compare(Ride ride1, Ride ride2) {
				
					Integer ridedistance1 = ride1.recalcdistance;
					Integer ridedistance2 = ride2.recalcdistance;
					
					//ascending order
					//return ridedistance1.compareTo(ridedistance2);
					
					//descending order
					return ridedistance1.compareTo(ridedistance2);
				}
			  };

}

class Simulator {
	public int rows;
	public int cols;
	public int vehicles;
	public int rides;
	public int bonus;
	public int steps;
	public Simulator(int rows, int cols, int vehicles, int rides, int bonus, int steps) {
		this.rows = rows;
		this.cols = cols;
		this.vehicles = vehicles;
		this.rides = rides;
		this.bonus = bonus;
		this.steps = steps;
	}
  
}
